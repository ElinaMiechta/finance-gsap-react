import React, { useEffect, useRef, useState } from "react";
import TextField from "@material-ui/core/TextField";
import { ButtonPrimaryContained } from "../elements/Button";
import { makeStyles } from "@material-ui/core/styles";
import { ReactComponent as Scene } from "../../assets/animateSvg/scene-contact.svg";
import { useGsap } from "../utils/useGsap";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: "25ch",
      backgroundColor: "transparent",
      height: " 50px",
      color: "#111",
    },
  },
}));

const handleMouseEnter = (cardRef, titleRef, sectionRef, buttonRef) => {
  cardRef.current.style.transition = "none";
  titleRef.current.style.transform = "translateZ(50px)";
  sectionRef.current.style.transform = "translateZ(50px) rotateZ(45deg)";
  buttonRef.current.style.transform = "translateZ(100px)";
};

const handleMouseLeave = (cardRef, titleRef, sectionRef, buttonRef) => {
  cardRef.current.style.transition = "all 0.5s ease";
  cardRef.current.style.transform = "rotateY(0deg) rotateX(0deg)";
  titleRef.current.style.transform = "translateZ(0px)";
  buttonRef.current.style.transform = "translateZ(0px)";
  sectionRef.current.style.transform = "translateZ(0px) rotateZ(0deg)";
};

const ContactCard = () => {
  const classes = useStyles();
  const contactRef = useRef(null);
  const cardRef = useRef(null);
  const titleRef = useRef(null);
  const sectionRef = useRef(null);
  const buttonRef = useRef(null);
  const [smallScreenAnimaion, setSmallScreenAnimaion] = useState(false);
  const [mobile, setMobile] = useState(false);
  const { gsap, ScrollTrigger } = useGsap();

  const manageSmallScreenAnimation = () => {
    return window.innerWidth <= 768
      ? setSmallScreenAnimaion(true)
      : setSmallScreenAnimaion(false);
  };

  useEffect(() => {
    if (!smallScreenAnimaion) return;
    const human = sectionRef.current.children[0].getElementById("postac");
    gsap.set(human, { autoAlpha: 0 });
    gsap.registerPlugin(ScrollTrigger);

    gsap.fromTo(
      human,
      { x: "+=500" },
      {
        duration: 3.5,
        x: 0,
        autoAlpha: 1,
        scrollTrigger: {
          trigger: contactRef.current,
          start: `top ${mobile ? "20%" : "50%+=100"}`,
          toggleActions: "play none none reverse",
          end: `${mobile ? "+=300" : "+=200"}`,
          scrub: true,
        },
      }
    );
  }, [smallScreenAnimaion]);

  useEffect(() => {
    manageSmallScreenAnimation();
    if (window.innerWidth <= 768) {
      setMobile(true);
    } else {
      setMobile(false);
    }
    if (window.innerWidth <= 768) return;

    contactRef.current.addEventListener("mousemove", (e) => {
      let xPos = (window.innerWidth / 2 - e.pageX) / 25;
      let yPos = window.innerHeight / 2 / 25;
      cardRef.current.style.transform = `rotateY(${xPos}deg) rotateX(${yPos}deg)`;
    });
    contactRef.current.addEventListener(
      "mouseenter",
      handleMouseEnter(cardRef, titleRef, sectionRef, buttonRef)
    );
    contactRef.current.addEventListener(
      "mouseleave",
      handleMouseLeave(cardRef, titleRef, sectionRef, buttonRef)
    );

    return () => {
      contactRef.current.removeEventListener(
        "mouseenter",
        handleMouseEnter(cardRef, titleRef, sectionRef, buttonRef)
      );
      contactRef.current.removeEventListener(
        "mouseleave",
        handleMouseLeave(cardRef, titleRef, sectionRef, buttonRef)
      );
    };
  }, []);

  useEffect(() => {
    window.addEventListener("resize", () => {
      if (window.innerWidth <= 768) {
        setMobile(true);
      } else {
        setMobile(false);
      }
      manageSmallScreenAnimation();
    });
  }, []);

  return (
    <div className="contactWrapper">
      <div className="contact" ref={contactRef}>
        <div className="contact__card" ref={cardRef}>
          <div className="contact__section">
            <div className="contact__section--circle"></div>
            <div className="contact__section--image" ref={sectionRef}>
              {smallScreenAnimaion && <Scene />}
            </div>
          </div>
          <div className="contact__body">
            <div className="contact__body--heading">
              <h1 ref={titleRef}>Have any questions..?</h1>
              <p>
                Choose subscription option and our consultants will contact You
                in 5 minutes!
              </p>
            </div>
            <div className="contact__body--options">
              <button>Starter</button>
              <button>Classic</button>
              <button className="active">Premium</button>
            </div>
            <div className="contact__body--contact">
              <TextField
                className={classes.root}
                id="filled"
                label="Your Phone"
                variant="filled"
              />
              <div className="contact__body--contactButton" ref={buttonRef}>
                <ButtonPrimaryContained text="Contact" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContactCard;
