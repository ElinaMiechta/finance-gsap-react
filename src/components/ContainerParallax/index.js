import React from "react";
import { Parallax  } from "react-parallax";

const ContainerParallax = () => {
  return (
    <div className="parallax">
      <Parallax
        bgImage="https://images.unsplash.com/photo-1511883040705-6011fad9edfc?ixid=MnwxMjA3fDB8MHxzZWFyY2h8N3x8ZmluYW5jZXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60"
        renderLayer={(percentage) => (
          <div
            className="parallax__inner"
            style={{
              position: "absolute",
              background: `rgba(28, 34, 55, 0.9)`,
              left: "50%",
              top: "50%",
              width: "50%",
              height: "300px",
            }}>
            <h1 id="logoText">
              Fine<strong>flow is ...</strong>
            </h1>
            <p>
              sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat.
            </p>
          </div>
        )}></Parallax>
    </div>
  );
};

export default ContainerParallax;
