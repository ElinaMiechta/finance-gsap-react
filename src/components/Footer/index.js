import YouTubeIcon from "@material-ui/icons/YouTube";
import InstagramIcon from "@material-ui/icons/Instagram";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import TwitterIcon from "@material-ui/icons/Twitter";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import { ButtonPrimary } from "../elements/Button";
import FingerprintIcon from "@material-ui/icons/Fingerprint";
import {Link} from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: "25ch",
      backgroundColor: "transparent",
      color: '#fff'
    },
  },
}));

const Footer = () => {
  const classes = useStyles();
  return (
    <div className="footer">
      <div className="footer__upper">
        <div className="footer__info">
          <h3>
            Join our exclusive membership to recieve the lates news and trends
          </h3>
          <p>You can unsubscribe at any time.</p>
        </div>
        <div className="footer__form">
          <TextField className={classes.root} id="filled-basic" label="Your Email" variant="filled" />
          <ButtonPrimary text="Subscribe" />
        </div>
      </div>
      <div className="footer__center">
        <div className="footer__col">
          <h4>About Us</h4>
          <p>How it works</p>
          <p>Testimonials</p>
          <p>Careers</p>
          <p>Investors</p>
        </div>
        <div className="footer__col">
          <h4>Contact Us</h4>
          <p>Contact</p>
          <p>Support</p>
          <p>Destinations</p>
          <p>Sponsorships</p>
        </div>
        <div className="footer__col">
          <h4>Videos</h4>
          <p>Submit Video</p>
          <p>Ambassadors</p>
          <p>Agency</p>
          <p>Influencer</p>
        </div>
        <div className="footer__col">
          <h4>Social Media</h4>
          <p>Instagram</p>
          <p>Facebook</p>
          <p>Youtube</p>
          <p>Twitter</p>
        </div>
      </div>
      <div className="footer__bottom">
        <div className="footer__bottom--logo">
          <Link to="/" className="header__logo">
            <FingerprintIcon className="header__logo--icon" /> FINEFLOW
          </Link>
        </div>
        <div className="footer__bottom--rights">
          <span>FineFlow 2020</span>
        </div>
        <div className="footer__bottom--media">
          <span>
            <YouTubeIcon />
          </span>
          <span>
            <InstagramIcon />
          </span>
          <span>
            <LinkedInIcon />
          </span>
          <span>
            <LinkedInIcon />
          </span>
          <span>
            <TwitterIcon />
          </span>
        </div>
      </div>
    </div>
  );
};

export default Footer;
