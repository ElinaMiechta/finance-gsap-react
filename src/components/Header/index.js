import { useState, useRef, useEffect } from "react";
import { Link } from "react-router-dom";
import FingerprintIcon from "@material-ui/icons/Fingerprint";
import MenuIcon from "@material-ui/icons/Menu";
import CloseIcon from "@material-ui/icons/Close";
import { ButtonPrimary, ButtonMobile } from "../elements/Button";

const Header = () => {
  const [click, setClick] = useState(false);
  const [button, setButton] = useState(true);
  const [mobileShow, setMobileShow] = useState(false);
  const navLinks = useRef(null);

  const showBtn = () => {
    if (window.innerWidth <= 960) {
      setButton(false);
      setMobileShow(true);
    } else {
      if(click) setClick(false);
      setButton(true);
      setMobileShow(false);
    }
  };

  useEffect(() => {
    showBtn();
  }, []);

  useEffect(() => {
    if (mobileShow) {
      navLinks.current.classList.add("hide");
    } else {
      navLinks.current.classList.remove("hide");
    }
  }, [mobileShow]);

  useEffect(() => {
    if (mobileShow && !click) {
      navLinks.current.classList.add("hide");
    }

  }, [click, mobileShow]);

  window.addEventListener("resize", showBtn);
  return (
    <div className="header">
      <div className="header--container">
        <div className="header__left">
          <Link to="/" className="header__logo" onClick={() => setClick(false)}>
            <FingerprintIcon className="header__logo--icon" /> FINEFLOW
          </Link>
        </div>
        <div className="header__right">
          {mobileShow && (
            <div className="header__menuIcon" onClick={() => setClick(!click)}>
              {!click ? (
                <MenuIcon className="header__menuIcon--bars" />
              ) : (
                <CloseIcon className="header__menuIcon--bars close" />
              )}
            </div>
          )}

          <ul
            className={click ? "header__menu active" : "header__menu"}
            ref={navLinks}>
            <li>
              <Link
                className="header__menu--link"
                to="/"
                onClick={() => setClick(false)}>
                Home
              </Link>
            </li>
            <li>
              <Link
                className="header__menu--link"
                to="/"
                onClick={() => setClick(false)}>
                Offers
              </Link>
            </li>
            <li>
              <Link
                className="header__menu--link"
                to="/"
                onClick={() => setClick(false)}>
                Products
              </Link>
            </li>
            <li
              className="header__menu--btn"
              onClick={() => setButton(!button)}>
              {button ? (
                <Link className="header__menu--btnLink" to="/">
                  <ButtonPrimary text="SIGN UP" />
                </Link>
              ) : (
                <Link className="header__menu--btnLink" to="/">
                  <ButtonMobile text="SIGN UP" />
                </Link>
              )}
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Header;
