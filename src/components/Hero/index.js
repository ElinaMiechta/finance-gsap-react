import { ButtonPrimaryContained } from "../elements/Button";
import { Link } from "react-router-dom";

const Hero = ({
  lightBg,
  topLine,
  lightText,
  lightTextDesc,
  headline,
  description,
  buttonLabel,
  img,
  alt,
  imgStart,
  animate,
  AnimateObj,
}) => {
  return (
    <div
      className="heroSection"
      style={{ backgroundColor: `${lightBg ? "#fff" : "#1c2237"}` }}>
      <div className="heroSection--container">
        <div
          className="heroSection__row"
          style={{
            display: "flex",
            flexDirection: imgStart === "start" ? "row-reverse" : "row",
          }}>
          <div className="heroSection__col">
            <div className="heroSection__col--textWrapper">
              <div className="heroSection__col--topline">{topLine}</div>
              <h1
                className="heroSection__col--heading"
                style={{ color: `${lightText ? "#f7f8fa" : "#000"}` }}>
                {headline}
              </h1>
              <p
                className="heroSection__col--descr"
                style={{ color: `${lightTextDesc ? "#fff" : "#000"}` }}>
                {description}
              </p>
              <Link to="/signup">
                <ButtonPrimaryContained text={buttonLabel} />
              </Link>
            </div>
          </div>
          <div className="heroSection__imgWrapper">
            {animate ? (
              <AnimateObj id="animateObj" />
            ) : (
              <img
                src={img}
                alt={alt}
                className="heroSection__imgWrapper--img"
              />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Hero;
