import { useEffect, useRef } from "react";
import WhatshotIcon from "@material-ui/icons/Whatshot";
import { ButtonPrimaryContained } from "../elements/Button";
import BlurOnIcon from "@material-ui/icons/BlurOn";
import LoyaltyIcon from "@material-ui/icons/Loyalty";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import { useGsap } from "../utils/useGsap";
import { useLoader } from "../utils/useLoader";

const useStyles = makeStyles(() => ({
  root: {
    width: "5rem",
    height: "5rem",
    color: "#fff",
    margin: "-15px  0 24px 0",
  },
}));

const Pricing = () => {
  const classes = useStyles();
  const { gsap, ScrollTrigger } = useGsap();
  const { isLoading } = useLoader();
  const cardLeft = useRef(null);
  const cardCenter = useRef(null);
  const cardRight = useRef(null);
  const parentRef = useRef(null);

  useEffect(() => {
    if (isLoading) {
      const tl = gsap.timeline({ defaults: { ease: "power3.inOut" } });
      gsap.registerPlugin(ScrollTrigger);

      gsap.set([cardLeft.current, cardRight.current], { autoAlpha: 0 });

      tl.fromTo(
        cardLeft.current,
        { autoAlpha: 0, scale: 0.9 },
        { duration: 1.5, scale: 1, autoAlpha: 1 }
      ).fromTo(
        cardRight.current,
        { autoAlpha: 0, scale: 0.9 },
        { duration: 1.5, scale: 1, autoAlpha: 1 },
        "-=1.1"
      );

      ScrollTrigger.create({
        trigger: parentRef.current,
        animation: tl,
        start: "top 40%",
        toggleActions: "play none none reverse",
        end: "+=100",
      });

      return () => tl.clear();
    }
  }, []);

  return (
    <div className="pricing" ref={parentRef}>
      <h1 className="pricing__heading">Pricing</h1>
      <div className="pricing__container">
        <Link to="/sign-up" className="pricing__container--card" ref={cardLeft}>
          <div className="pricing__container--cardInfo">
            <div className="pricing__icon">
              <WhatshotIcon className={classes.root} />
            </div>
            <h3>Starter</h3>
            <h4>$13.99</h4>
            <p>per month</p>
            <ul className="pricing__container--features">
              <li>100 Transactions</li>
              <li>2% Cash Back</li>
              <li>$10 000 Limit</li>
            </ul>
            <ButtonPrimaryContained text="Choose Plan" />
          </div>
        </Link>

        <Link
          to="/sign-up"
          className="pricing__container--card"
          ref={cardCenter}>
          <div className="pricing__container--cardInfo">
            <div className="pricing__icon">
              <BlurOnIcon className={classes.root} />
            </div>
            <h3>Classic</h3>
            <h4>$18.99</h4>
            <p>per month</p>
            <ul className="pricing__container--features">
              <li>1000 Transactions</li>
              <li>4.5% Cash Back</li>
              <li>$50 000 Limit</li>
            </ul>
            <ButtonPrimaryContained text="Choose Plan" />
          </div>
        </Link>

        <Link
          to="/sign-up"
          className="pricing__container--card"
          ref={cardRight}>
          <div className="pricing__container--cardInfo">
            <div className="pricing__icon">
              <LoyaltyIcon className={classes.root} />
            </div>
            <h3>Premium Loyality</h3>
            <h4>$25.99</h4>
            <p>per month</p>
            <ul className="pricing__container--features">
              <li>Unlimited Transactions</li>
              <li>4.5% Cash Back</li>
              <li>$100 000 Limit</li>
            </ul>
            <ButtonPrimaryContained text="Choose Plan" />
          </div>
        </Link>
      </div>
    </div>
  );
};

export default Pricing;
