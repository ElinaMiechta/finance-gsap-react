import {useEffect} from 'react';
import gsap from 'gsap';

const  Animation = () => {
      useEffect(() => {
        const elem = document.querySelectorAll('.frame');
        const text = elem[0].firstChild;
        const tl = gsap.timeline({ defaults: { ease: "power3.inOut" } });
        gsap.set([elem, text], {autoAlpha:0});
    
        tl.fromTo([elem[0],elem[1], elem[2]], {scale: 0.8}, {duration:1.5, scale:1, autoAlpha:1})
        .to(elem[1], {duration: 0.7, x: -100, y: 100, borderColor: '#edbe51'})
        .to(elem[2], {duration: 0.7, x: 100, y: -100, borderColor: '#a2c70e'}, '-=0.7')
        .to(elem[0], {duration: 1, borderColor: '#6854cc'}, '-=0.7')
    
        .to(elem[1], {duration: 1.1, x: 0, y: 0, borderColor: '#fff'})
        .to(elem[2], {duration: 1.1, x: 0, y: 0, borderColor: '#fff'}, '-=1.1')
        .to(elem[0], {duration: 1.1, x: 0, y: 0, borderColor: '#fff'}, '-=0.5')
    
        .fromTo(elem[1], {background: 'transparent'}, {duration: 0.7, x: -300,background: '#122336', borderColor: 'transparent', zIndex:1})
        .fromTo(elem[2],{background: 'transparent'}, {duration: 0.7, x: 300, background: '#122336',zIndex:1, borderColor: 'transparent'}, '-=0.7')
        .to(text, {durations: 1, autoAlpha:1}, '-=0.4')
    
        .to(elem[1], {duration: 1.5, x: 0})
        .to(elem[2], {duration: 1.5, x: 0}, '-=1.5');

        return () => tl.clear();
    
      }, [])
    
      return (
        <>
        <div className="animation">
            <div className="frame">
            <span style={{fontWeight:'800', color: '#fff'}}>FINE<strong>flow.</strong></span>
            </div>
            <div className="frame"></div>
            <div className="frame"></div>
            </div>
        </>
      );
}

export default Animation
