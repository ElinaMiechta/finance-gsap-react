import { Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    background: "transparent",
    color: "#fff",
    padding: "8px 20px",
    border: "1px solid #fff",
    transition: "all .3s ease-out",
  },
  contained: {
    background: "#577ef2",
    color: "#fff",
    border: 'none',
  },
  mobile: {
    background: "transparent",
    textAlign: "center",
    width: "80%",
    fontSize: "1.5rem",
    color: "#fff",
    padding: "14px 20px",
    border: "1px solid #fff",
  },
});

export const ButtonPrimary = ({ text, type, onClick }) => {
  const classes = useStyles();
  return (
    <Button
      className={classes.root}
      variant="outlined"
      type={type}
      onClick={onClick}>
      {text}
    </Button>
  );
};

export const ButtonPrimaryContained = ({ text, type, onClick }) => {
  const classes = useStyles();
  return (
    <Button
      className={classes.contained}
      variant="contained"
      type={type}
      onClick={onClick}>
      {text}
    </Button>
  );
};

export const ButtonMobile = ({ text, type, onClick }) => {
  const classes = useStyles();
  return (
    <Button
      className={classes.mobile}
      variant="outlined"
      type={type}
      onClick={onClick}>
      {text}
    </Button>
  );
};
