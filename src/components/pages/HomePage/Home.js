import React, { useRef, useEffect } from "react";
import Hero from "../../Hero";
import { homeObj } from "./data";
import Animation from "../../elements/Animation";
import Pricing from '../../Pricing';
import {useGsap} from '../../utils/useGsap';
import {useLoader} from '../../utils/useLoader';
import ContainerParallax from '../../ContainerParallax';
import ContactCard from '../../Contact';

const Home = () => {
  const {gsap, ScrollTrigger} = useGsap();
  const {isLoading, setEndOfLoading} = useLoader();

  const revealRefs = useRef([]);
  revealRefs.current = [];

  const addToRefs = (el) => {
    if (el && !revealRefs.current.includes(el)) {
      revealRefs.current.push(el);
    }
  };

  useEffect(() => {

    if (!isLoading) {
    const tl = gsap.timeline({ defaults: { ease: "power3.inOut" } });

    gsap.registerPlugin(ScrollTrigger);

    const heroSectionColumn = document.querySelectorAll(".heroSection__col"),
      notes = document.querySelectorAll(".heroSection__imgWrapper")[2]
        .children[0],
      papers = document.querySelectorAll(".heroSection__imgWrapper")[3]
        .children[0];

    gsap.set(
      [
        notes,
        papers,
        heroSectionColumn[0],
        heroSectionColumn[1],
        heroSectionColumn[2],
        heroSectionColumn[3],
      ],
      { autoAlpha: 0 }
    );

    tl.fromTo(
      heroSectionColumn[0],
      { y: "+=50" },
      { duration: 2, y: 0, autoAlpha: 1 }
    )
    .addLabel('notes')
      .fromTo(
        notes,
        { y: "+=50", autoAlpha: 0 },
        {
          duration: 4,
          y: 0,
          autoAlpha: 1,
          scrollTrigger: {
            trigger: notes,
            start: "top 50%+=100",
            toggleActions: "play none none reverse",
            end: "+=200",
            scrub: true,
          },
        }
      )
      .addLabel('papers')
      .fromTo(
        papers,
        { y: "+=50", autoAlpha: 0 },
        {
          duration: 4,
          y: 0,
          autoAlpha: 1,
          scrollTrigger: {
            trigger: papers,
            start: "top 50%+=100",
            toggleActions: "play none none reverse",
            end: "+=200",
            scrub: true,
          },
        }
      );

    heroSectionColumn.forEach((el, index) => {
      if (index === 0) return;
      tl.fromTo(
        el,
        { y: "+=50", autoAlpha: 0 },
        {
          duration: 4,
          y: 0,
          autoAlpha: 1,
          scrollTrigger: {
            trigger: el,
            start: "top 50%",
            toggleActions: "play none none reverse",
            end: "+=200",
            scrub: true,
          },
        }
      );
    });

    return () => tl.clear();
  }

  }, [isLoading]);

  useEffect(() => {
    if(!isLoading) {
    const secureContainer = document.querySelectorAll(
      ".heroSection__imgWrapper"
    )[1].children[0];

    const secureIcon = secureContainer.getElementById("security"),
      flowersRight = secureContainer.getElementById("rightFlowers");
    const tl = gsap.timeline({ defaults: { ease: "power3.in" } });
    gsap.set([secureIcon, flowersRight], { autoAlpha: 0 });
    gsap.set(flowersRight, { transformOrigin: "50% 100%" });
    tl.fromTo(
      secureIcon,
      { y: "+=10", scale: 0.9 },
      { duration: 1.5, y: 0, scale: 1, autoAlpha: 1 }
    ).fromTo(
      flowersRight,
      { scaleY: 0 },
      { duration: 1, scaleY: 1, autoAlpha: 1 },
      "-=0.4"
    );

    ScrollTrigger.create({
      trigger: secureContainer,
      animation: tl,
      start: "top 50%",
      end: "+=200",
      scrub: true,
    });

    return () => tl.clear();
  }
  }, [isLoading]);

  useEffect(() => {
   const timeout = setTimeout(() => {
    setEndOfLoading();
   }, 7000);

   return () => clearTimeout(timeout);
  }, []);

  useEffect(() => {
    if (!isLoading) {
      const elements = document.querySelectorAll(".heroSection__imgWrapper")[0]
        .children[0];
      const card1 = elements.getElementById("card1"),
        card2 = elements.getElementById("card2"),
        beans1 = elements.getElementById("beans1"),
        beans2 = elements.getElementById("beans2"),
        beans3 = elements.getElementById("beans3"),
        beans4 = elements.getElementById("beans4"),
        lines = elements.getElementById("horizontalLines");

      gsap.set([card1, card2, beans1, beans2, beans3, beans4, lines], {
        autoAlpha: 0,
      });
      gsap.set(card2, { zIndex: -1 });

      const tl = gsap.timeline({
        defaults: { ease: "power3.inOut" },
        delay: 0.5,
      });
      tl.to(card1, { duration: 2, zIndex: 1, autoAlpha: 1 })
        .fromTo(
          lines,
          { x: "-=20" },
          { duration: 1.5, x: "+=40", autoAlpha: 1 },
          "-=1.5"
        )

        .fromTo(
          beans1,
          { scale: 0.9 },
          { duration: 1.3, scale: 1, autoAlpha: 1 },
          "-=1.3"
        )
        .fromTo(
          beans2,
          { scale: 0.9 },
          { duration: 1.3, scale: 1, autoAlpha: 1 },
          "-=1"
        )
        .fromTo(
          beans3,
          { scale: 0.9 },
          { duration: 1.3, scale: 1, autoAlpha: 1 },
          "-=1"
        )
        .fromTo(
          beans4,
          { scale: 0.9 },
          { duration: 1.3, scale: 1, autoAlpha: 1 },
          "-=1"
        )

        .fromTo(
          card2,
          { y: "+=10", rotate: "30deg" },
          { duration: 1.5, y: "-=10", rotate: "0deg", autoAlpha: 1 },
          "-=1.5"
        );
      return () => tl.clear();
    }
  }, [isLoading]);

  return (
    <>
      {isLoading ? (
        <Animation />
      ) : (
        <>
          {homeObj.map((el) => (
            <div ref={addToRefs}>
              <Hero key={el.headline} {...el} AnimateObj={el.animateObj} />
            </div>

          ))}
            <Pricing/>
            <ContainerParallax/>
            <ContactCard />

        </>
      )}
    </>
  );
};

export default Home;
