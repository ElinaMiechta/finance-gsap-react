import gsap from 'gsap';
import { ScrollTrigger } from "gsap/ScrollTrigger";

export const useGsap = () => {

      return {gsap, ScrollTrigger};


};