import {useState  } from 'react';

export const useLoader = () => {
      const [isLoading, setIsLoading] = useState(true);


      const setEndOfLoading = () =>  setIsLoading(false);
     
      

      return {isLoading, setEndOfLoading}
}